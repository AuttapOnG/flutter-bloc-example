import 'package:flutter/material.dart';
import 'package:flutter_bloc_example/modules/home/bloc/home_bloc.dart';
import 'package:provider/provider.dart';

import 'modules/home/ui/home_page.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (BuildContext context) => HomeBloc(),
        ),
      ],
      child: MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage(
          title: "Flutter bloc",
        ),
      ),
    );
  }
}

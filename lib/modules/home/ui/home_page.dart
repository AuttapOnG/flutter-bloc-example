import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc_example/modules/home/bloc/home_bloc.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late HomeBloc _homeBloc;
  String message = '';

  void initState() {
    super.initState();
    _homeBloc = Provider.of<HomeBloc>(context, listen: false);
    _homeBloc.messageStream.listen((event) {
      if (mounted) {
        setState(() {
          message = '$event';
        });
      }
    });
  }

  void _incrementCounter() {
    _homeBloc.updateMessage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$message',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

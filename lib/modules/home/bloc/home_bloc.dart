import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc_example/core_components/const/env.dart';
import 'package:flutter_bloc_example/core_components/core_components.dart';

class HomeBloc extends Bloc with ChangeNotifier {
  final _messageStreamController = StreamController<int>.broadcast();

  Stream<int> get messageStream => _messageStreamController.stream;

  int count = 0;

  void updateMessage() {
    debugPrint(API_BASE_URL);
    count = count + 1;
    _messageStreamController.add(count);
    notifyListeners();
  }
}

import 'dart:async';

abstract class Bloc<T> {
  late T _t;
  final _streamController = StreamController<T>.broadcast();
  Stream<T> get stream => _streamController.stream;
  T get state => _t;

  void emit(T data) {
    _t = data;
    _streamController.sink.add(data);
  }

  void emitError(String error) {
    _streamController.sink.addError(error);
  }

  void dispose() {
    _streamController.close();
  }
}

import 'package:package_info/package_info.dart';
import 'package:flutter_bloc_example/core_components/app_constants.dart';

Future<Map<String, String>> getRegisterServiceHeader(
  Map<String, String> headers,
) async {
  String appVersion = await PackageInfo.fromPlatform().then(
    (value) => value.buildNumber,
  );
  headers[AppConstants.APP_VERSION] = appVersion;
  return headers;
}
